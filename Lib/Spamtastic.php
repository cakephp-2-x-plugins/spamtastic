<?php
App::import('Vendor', 'Spamtastic.b8', array('file' => 'b8-0.6.1/b8/b8.php'));

/**
 * Convenience wrapper for b8.php.
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @license LGPL
 * @version 1.0
 */
class Spamtastic {
	protected static $settings = array(
			'b8' => array('storage' => 'mysqli'),
			'storage' => array(
					'table_name' => 'b8_wordlist',
					'host' => 'localhost',
					'user' => '',
					'pass' => '',
					'database' => ''),
			'lexer' => array('old_get_html' => FALSE, 'get_html' => TRUE),
			'degenerator' => array('multibyte' => TRUE));
	protected static $b8 = false;

	/**
	 * Create a new b8 instance to work with.
	 *
	 * @return b8 instance
	 *
	 * @since 1.0
	 */
	protected static function getInstance() {
		if (self::$b8) {
			return self::$b8;
		}

		// Load database configuration
		$db = new DATABASE_CONFIG();
		self::$settings['storage']['host'] = $db->default['host'];
		self::$settings['storage']['user'] = $db->default['login'];
		self::$settings['storage']['pass'] = $db->default['password'];
		self::$settings['storage']['database'] = $db->default['database'];

		// Create a new b8 instance
		try {
			self::$b8 = new b8(self::$settings['b8'], self::$settings['storage'],
					self::$settings['lexer'], self::$settings['degenerator']);
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
			self::$b8 = false;
		}

		return self::$b8;
	}

	/**
	 * Rate a piece of text as SPAM or HAM.
	 * Values closer to 0 are HAM and closer to 1 are SPAM.
	 *
	 * @param string $text
	 *        	Text to classify.
	 * @return float Rating or false on failure.
	 * @since 1.0
	 */
	public static function classify($text) {
		$b8 = self::getInstance();
		if (!$b8) {
			return false;
		}

		return $b8->classify($text);
	}

	/**
	 * Store a string as HAM text.
	 *
	 * @param string $text
	 *        	Text to learn as HAM.
	 * @return boolean Success
	 * @since 1.0
	 */
	public static function ham($text) {
		$b8 = self::getInstance();
		if (!$b8) {
			return false;
		}

		return $b8->learn($text, b8::HAM);
	}

	/**
	 * Store a string as SPAM text.
	 *
	 * @param string $text
	 *        	Text to learn as SPAM.
	 * @return boolean Success
	 * @since 1.0
	 */
	public static function spam($text) {
		$b8 = self::getInstance();
		if (!$b8) {
			return false;
		}

		return $b8->learn($text, b8::SPAM);
	}
}