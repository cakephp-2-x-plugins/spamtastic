<?php
App::uses('B8Wordlist', 'Spamtastic.Model');
App::uses('ClassRegistry', 'Utility');

/**
 *
 * @author Jason Burgess <jason@notplugged.in>
 *
 */
class SpamtasticSchema extends CakeSchema {
	public function after($event = array()) {
		if (isset($event['create'])) {
			switch ($event['create']) {
				case 'b8_wordlist':
					$item = ClassRegistry::init(
							array('class' => 'B8Wordlist', 'table' => 'b8_wordlist'));
					$item->create();
					$item->save(
							array(
									'B8Wordlist' => array(
											'token' => 'b8*dbversion',
											'count_ham' => 3)));
					$item->create();
					$item->save(
							array(
									'B8Wordlist' => array(
											'token' => 'b8*text',
											'count_ham' => 0,
											'count_spam' => 0)));

					break;
			}
		}
	}

	/**
	 *
	 * @var unknown
	 */
	public $b8_wordlist = array(
			'token' => array(
					'type' => 'string',
					'null' => false,
					'default' => null,
					'length' => 255,
					'key' => 'primary',
					'collate' => 'utf8_general_ci',
					'charset' => 'utf8'),
			'count_ham' => array('type' => 'integer', 'null' => false, 'default' => null),
			'count_spam' => array('type' => 'integer', 'null' => false, 'default' => null),
			'indexes' => array('PRIMARY' => array('column' => 'token', 'unique' => 1)),

			'tableParameters' => array(
					'charset' => 'utf8',
					'collate' => 'utf8_general_ci',
					'engine' => 'MyISAM'));
}