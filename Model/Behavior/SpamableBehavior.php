<?php
App::uses('ModelBehavior', 'Model');
App::uses('Spamtastic', 'Spamtastic.Lib');

/**
 *
 * @author Jason Burgess <jason@notplugged.in>
 *
 */
class SpamableBehavior extends ModelBehavior {
	/**
	 * Note: separator can be several characters and will work with all of them.
	 *
	 * @var array Default settings for TagBehavior.
	 */
	public $defaultSettings = array(
			'textField' => array('text'),
			'spamField' => 'spam',
			'maxRating' => '0.75');

	/**
	 * Initiate behavior for the model using specified settings.
	 *
	 * @param object $Model
	 *        	Model using the behavior
	 * @param array $settings
	 *        	Settings to override for model.
	 *
	 * @access public
	 *
	 * @see Cake.ModelBehavior::setup()
	 */
	public function setup(Model $Model, $settings = array()) {
		if (empty($this->settings[$Model->alias])) {
			$this->settings[$Model->alias] = $this->defaultSettings;
		}

		if (is_array($settings)) {
			$this->settings[$Model->alias] = array_merge($this->settings[$Model->alias], $settings);
		}
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ModelBehavior::beforeSave()
	 */
	public function beforeSave(Model $Model, $options = array()) {
		$settings = array_merge($this->settings[$Model->alias], $options);

		if ($Model->hasField($settings['spamField']) &&
				 empty($Model->data[$Model->alias][$Model->primaryKey])) {
			$text = $Model->getText($Model->data, $settings);

			if ($text) {
				$rating = Spamtastic::classify($text);

				// If it is higher than the max rating, mark it as spam automatically.
				if ($rating > $settings['maxRating']) {
					$Model->data[$Model->alias][$settings['spamField']] = true;
				}
			}
		}

		return true;
	}

	/**
	 * Get all text fields specified as one text string.
	 *
	 * @param Model $Model
	 * @param array $data
	 * @return string Text to check
	 */
	public function getText(Model $Model, $data = false) {
		$settings = $this->settings[$Model->alias];

		if (!$data) {
			$data = $Model->data;
		}

		if (!is_array($settings['textField'])) {
			$settings['textField'] = array($settings['textField']);
		}

		$text = '';
		foreach ($settings['textField'] as $field) {
			if ($Model->hasField($field) && !empty($data[$Model->alias][$field])) {
				$text .= $data[$Model->alias][$field] . "\n";
			}
		}

		return $text;
	}

	/**
	 * Learn contents as SPAM.
	 *
	 * @param Model $Model
	 * @param array $data
	 * @return boolean Success
	 */
	public function learnSpam(Model $Model, $data = false) {
		if (!$data) {
			$data = $Model->data;
		}

		$text = $Model->getText($data);

		if ($text) {
			return Spamtastic::spam($text);
		}

		return false;
	}

	/**
	 * Learn contents as HAM.
	 *
	 * @param Model $Model
	 * @param array $data
	 * @return boolean Success
	 */
	public function learnHam(Model $Model, $data = false) {
		if (!$data) {
			$data = $Model->data;
		}

		$text = $Model->getText($data);

		if ($text) {
			return Spamtastic::ham($text);
		}

		return false;
	}
}